/*
  Provides a client interface for unsplash images.
*/
import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: {
    Authorization: 'Client-ID c6c5bf34921469c98b4fbd3f74511eed83dd7132820ef6c7faab666fb7ae7229'
  }
});
