import React from 'react';
import ImageList from './ImageList';
import SearchBar from './SearchBar';
import unsplash from '../api/unsplash';

class App extends React.Component {
  state = {
    images: []
  };

  handleSearchSubmit = async (text) => {
    const response = await unsplash.get('/search/photos', {
      params: {query: text}        
    });
    this.setState({ images: response.data.results });
  }

  render() {
    return(
      <div className="ui container" style={{ marginTop: '10px' }}>
        <SearchBar onSubmit={ this.handleSearchSubmit } />
        <ImageList images={this.state.images} />
      </div>
    );
  }
}

export default App;