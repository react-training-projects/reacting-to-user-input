import React from 'react';

class ImageCard extends React.Component {
  constructor(props) {
    super(props)

    this.state = { spans: 0 };

    this.imageRef = React.createRef();
  }

  componentDidMount() {
    this.imageRef.current.addEventListener('load', this.setSpans);
  }

  setSpans = () => {
    var height = this.imageRef.current.clientHeight;
    var span = parseInt(height / 10) + 1;

    this.setState({ spans: span });
  }

  render() {
    const {desc, urls} = this.props.img;

    return(
      <div 
        className="image-card" 
        style={{ gridRowEnd: `span ${this.state.spans}` }}
      >
        <img ref={this.imageRef} alt={desc} src={urls.regular} />
      </div>
    );
  }
}

export default ImageCard;
