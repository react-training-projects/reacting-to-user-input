import React from 'react';

class SearchBar extends React.Component {
  /*
    Setting state marks the difference between controlled and
    uncontrolled components. With state, this is a controlled
    component.

    Typically, values on-screen are set via state, so
    interactions can be controlled via handlers on change.
  */
  state = {
    text: ''
  };

  handleSubmit = (event) => {
    event.preventDefault();

    this.props.onSubmit(this.state.text);
  };

  render() {
    return (
      <div className="SearchBar ui segment">
        <form 
          className="ui form"
          onSubmit={ this.handleSubmit }
        >
          <div className="field">
            <label htmlFor="search">Image Search:</label>
            <input 
              type="text" 
              id="search" 
              value={this.state.text}
              onChange={(e) => {
                this.setState({ text: e.target.value })
              }}
            />
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBar;
